Bu proje cihazımızın kullandığı iletişim kartlarını listeleyerek bu kartları kullanarak ağ üzerinden paket alışverişi yapan kartları tespit edip bu paketlerin yakalanması amacı ile oluşturulmuştur.
Yakalanan paketlerin bilgisi tablo şeklinde kullanıcıya gösterilmektedir. 
Lütfen yazılımı yönetici olarak çalıştırın ve öyle kullanın.
