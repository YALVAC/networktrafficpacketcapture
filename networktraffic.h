#ifndef NETWORKTRAFFIC_H
#define NETWORKTRAFFIC_H

#include <QMainWindow>
#include <iostream>
#include "pcap.h"
#include "stdio.h"
#include "stdlib.h"
#include "pcap-bpf.h"
#include "qdebug.h"
#include "netinet/in.h"
#include "netinet/ether.h"
#include "net/ethernet.h"
#include <QByteArray>
#include <QTableWidget>
#include "netinet/ether.h"
#include "net/ethernet.h"
#include "sys/socket.h"
#include "netinet/ether.h"
#include "netinet/if_ether.h"
#include "arpa/inet.h"
#include "unistd.h"
#include "QTimer"
#include <QTableWidget>
#include <QDateTime>
#include <QStorageInfo>
#include <QNetworkInterface>
#include <QSettings>
#include <QProcess>
#include <QDebug>
#include <QTableWidgetItem>
#include <QListWidgetItem>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QHostAddress>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class NetworkTraffic; }
QT_END_NAMESPACE

class NetworkTraffic : public QMainWindow
{
	Q_OBJECT

public:

	NetworkTraffic(QWidget *parent = nullptr);
	~NetworkTraffic();

private slots:
	void threadCreate();
	void pCap(QString device);
	void TableWidget();
	void protocolControl();
	void getPacket();
	void displayAllDevice();
	void colorControl();
	void on_stop_clicked();
	void on_clear_clicked();
	void on_deviceListWidget_itemClicked(QListWidgetItem *item);
	void on_continue_2_clicked();

private:
	QThread *thread;
	int count;
	int filterCount;
	int capLen;
	bool stop;
	bool clear;
	int countinueClickNumber;


	QString strEthernetLength;
	QTableWidget *table;
	QString sourcePort;
	QString destinationPort;

	QTableWidgetItem *item;
	QString strTime;
	QStringList deviceList;
	pcap_t *pcap; //pcap handle işaretçisi
	pcap_t *pcapControl;
	struct ether_header *pEthHeader;
	struct ether_arp *arpEth;
	struct ether_addr *pEthAddr;
	typedef struct IpPacket {
		u_char headerLen : 4; /* 4bit başlık uzunluğu */
		u_char version : 4; /* 4bit versiyon numarası */
		u_short totalLen; /* Toplam uzunluk */
		u_short id; /* paket idsi */
		u_short offset; /* ofset */
		u_char ttl; /* TTL */
		u_char protocol; /* protokol */
		struct in_addr srcIp; /* kaynak ip adresi*/
		struct in_addr dstIp; /* hedef ip adresi */
		struct ethhdr *ethdr;
		u_int op_pad; // opsiyon bölümü
	} IpPacket;
	IpPacket *pIpPacket;

	typedef struct TcpPacket {
		u_short srcPort; /* 2 bayt kaynak port no*/
		u_short dstPort; /* 2 bayt hedef port no */
		u_int seqNum; /* 4 byte seq no */
		u_int ackNum; /* 4 byte ack no */
		int headerLen;
		u_char flags;
		u_short window;
		u_short checkSum;
		u_short urgentPointer;
	}TcpPacket;
	TcpPacket *pTcpPacket;
	struct arpd_request *arpReq;
	struct arpreq_old *arpReqOld;
	#define IP_HEADER_LEN(pIpPacket) \
	(((u_short)((((IpPacket *)pIpPacket)->headerLen)) * 4))
	#define TCP_PACKET(pIpPacket) \
	((TcpPacket *)(((u_char *)pIpPacket) + IP_HEADER_LEN(pIpPacket)))
	Ui::NetworkTraffic *ui;
};
#endif // NETWORKTRAFFIC_H
