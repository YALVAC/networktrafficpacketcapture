#include "networktraffic.h"
#include "ui_networktraffic.h"
#include <QThread>
#include <QSize>
#include <QHeaderView>


NetworkTraffic::NetworkTraffic(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::NetworkTraffic)
{
	ui->setupUi(this);
	displayAllDevice();
	countinueClickNumber =0;
	filterCount=0;
	table = ui->tableWidget;
	table->setColumnCount(6);
	QStringList upLabels;
	upLabels<<"Date Time:"<<"Source"<<"Destination"
		   <<"Protocol"<<"Length"<<"INFO";
	table->setHorizontalHeaderLabels(upLabels);
	table->setColumnWidth(0,400);
	table->setColumnWidth(1,300);
	table->setColumnWidth(2,300);
	table->setColumnWidth(3,200);
	table->setColumnWidth(4,100);
	table->setColumnWidth(5,300);
}

void NetworkTraffic::pCap(QString device)
{
	count=0;
	table->setRowCount(count);
	QByteArray ba = device.toLatin1();
	if(!ba.size())
		return;
	const char *pDev = ba.data();
	char errbuff[PCAP_ERRBUF_SIZE];
	pcap = pcap_open_live(pDev, BUFSIZ, 1, 1000, errbuff);
	ui->warning->setText("Device Capturing");
	if(pcap==NULL){
		ui->warning->setText("Device Can Not Capturing");
			thread->terminate();
	return;
	}
	threadCreate();
}

void NetworkTraffic::TableWidget()
{
	if(clear==true)
	{
		count=0;
		table->setRowCount(count);
		clear = false;
		return;
	}
	{
		item = new QTableWidgetItem();
		item->setText(strTime);
		colorControl();
		table->setItem(count,0,item);
	}
	{
		item = new QTableWidgetItem();
		if(ntohs(pEthHeader->ether_type)==ETHERTYPE_IP)
			item->setText(QString::fromLocal8Bit(inet_ntoa(pIpPacket->srcIp)));
		else{
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr*)pEthHeader->ether_shost)));
		}

		colorControl();
		table->setItem(count,1,item);
	}
	{
		item = new QTableWidgetItem();
		if(ntohs(pEthHeader->ether_type)==ETHERTYPE_IP)
			item->setText(QString::fromLocal8Bit(inet_ntoa(pIpPacket->dstIp)));
		else{
			item->setText(QString::fromLocal8Bit(ether_ntoa((const ether_addr*)pEthHeader->ether_dhost)));
			if(QString::fromLocal8Bit(ether_ntoa((const ether_addr*)pEthHeader->ether_dhost))=="ff:ff:ff:ff:ff::ff");
			item->setText("BROADCAST");
		}
		colorControl();
		table->setItem(count,2,item);
	}
	protocolControl();

	{
		item = new QTableWidgetItem();
		item->setText(strEthernetLength);
		colorControl();
		table->setItem(count,4,item);
	}
	{
		item = new QTableWidgetItem();
		item->setText("[" + sourcePort +"  ->  "+ destinationPort + "]");
		QString::number(ntohs(pTcpPacket->dstPort));
		colorControl();
		table->setItem(count,5,item);
	}
	count++;
	QString Filter = ui->filterLine->text();
	if(!Filter.isEmpty() || filterCount==1){
		filterCount=1;
		for(int i = 0; i<count; i++){
			bool match = true;
			for(int j= 0; j<table->columnCount(); j++){
				item = table->item(i,j);
				if(item->text().contains(Filter))
					match = false;
			}
			table->setRowHidden(i,match);
		}
	}
	if(Filter.isEmpty()){
		filterCount=0;
		return;
	}
}

NetworkTraffic::~NetworkTraffic()
{
	delete ui;
}

void NetworkTraffic::threadCreate()
{
	thread = QThread::create([this]{
		getPacket();
	});
	thread->start();
}

void NetworkTraffic::getPacket()
{
	while(1){
		table->setRowCount(count+1);
		struct pcap_pkthdr pktHdr;


		const u_char *pktAddr= pcap_next(pcap, &pktHdr);

		arpEth = (struct ether_arp*)pktAddr;

		pIpPacket = (IpPacket *)(pktAddr + ETHER_HDR_LEN);
		pEthHeader = (struct ether_header *)pktAddr;
		pTcpPacket = (TcpPacket *)TCP_PACKET(pIpPacket);
		int intLength = pktHdr.len;
		strEthernetLength = QString::number(intLength);
		sourcePort = QString::number(ntohs(pTcpPacket->srcPort));
		destinationPort = QString::number(ntohs(pTcpPacket->srcPort));
		int timeInt;
		timeInt= pktHdr.ts.tv_sec;
		QDateTime time;
		time.setTime_t(timeInt);
		strTime = time.toString(Qt::SystemLocaleLongDate);
		TableWidget();
		if(stop==true)
			return;
	}
}

void NetworkTraffic::displayAllDevice()
{
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *alldevs;
	if (pcap_findalldevs(&alldevs, errbuf)!=0)
	{
		QMessageBox::information(this, tr("ERROR"), tr(errbuf));
		exit(EXIT_FAILURE);
	}

	for(;alldevs != NULL; alldevs = alldevs->next){
		deviceList.append(alldevs->name);
	}
	ui->deviceListWidget->addItems(deviceList);
	pcap_freealldevs(alldevs);
}

void NetworkTraffic::colorControl()
{
		if (pIpPacket->protocol==IPPROTO_TCP)
			item->setData(Qt::BackgroundRole, QColor("#99FFFF"));
		else if (pIpPacket->protocol==IPPROTO_UDP)
			item->setData(Qt::BackgroundRole, QColor("#00FF00"));
		else if (ntohs(pEthHeader->ether_type)==ETHERTYPE_IP)
			item->setData(Qt::BackgroundRole, QColor("#FF6699"));
		else if (ntohs(pEthHeader->ether_type)==ETHERTYPE_ARP)
			item->setData(Qt::BackgroundRole, QColor("#FFFFCC"));
		else if (ntohs(pEthHeader->ether_type)==ETHERTYPE_IPV6)
			item->setData(Qt::BackgroundRole, QColor("#99FFFF"));
		else if (ntohs(pEthHeader->ether_type)==ETHERTYPE_PUP)
			item->setData(Qt::BackgroundRole, QColor("#FF9999"));
		else if (ntohs(pEthHeader->ether_type)==ETHERTYPE_SPRITE)
			item->setData(Qt::BackgroundRole, QColor("#99CC66"));
		else if (pIpPacket->protocol==IPPROTO_EGP)
			item->setData(Qt::BackgroundRole, QColor("#CCFFCC"));
		else if (pIpPacket->protocol==IPPROTO_ICMP)
			item->setData(Qt::BackgroundRole, QColor("#CC9966"));
		else if (pIpPacket->protocol==IPPROTO_DCCP)
			item->setData(Qt::BackgroundRole, QColor("#00FFFF"));
		else if (pIpPacket->protocol==IPPROTO_IGMP)
			item->setData(Qt::BackgroundRole, QColor("#00FFFF"));
		else
			item->setData(Qt::BackgroundRole, QColor("#FFFFFF"));
}

void NetworkTraffic::protocolControl()
{
	item = new QTableWidgetItem();
	QString transportProtocol;
	if(IPPROTO_UDP==pIpPacket->protocol)
		transportProtocol = "UDP";
	else if(IPPROTO_TCP==pIpPacket->protocol)
		transportProtocol = "TCP";
	if(ntohs(pEthHeader->ether_type)==ETHERTYPE_PUP)
		item->setText(transportProtocol + ":PUP");
	else if(ntohs(pEthHeader->ether_type)==ETHERTYPE_SPRITE)
		item->setText(transportProtocol + ":SPRRITE");
	else if(ntohs(pEthHeader->ether_type)==ETHERTYPE_IP){
		item->setText(transportProtocol + ":IP");
	if(pIpPacket->protocol==IPPROTO_ICMP)
		item->setText(transportProtocol+":ICMP");
	if(pIpPacket->protocol==IPPROTO_DCCP)
		item->setText(transportProtocol +":DCCP");
	if(pIpPacket->protocol==IPPROTO_IGMP)
		item->setText(transportProtocol+ ":IGMP");
	if(pIpPacket->protocol==IPPROTO_EGP)
		item->setText(transportProtocol +":EGP");
	}
	else if(ntohs(pEthHeader->ether_type)==ETHERTYPE_ARP)
		item->setText(transportProtocol + ":ARP");
	else if(ntohs(pEthHeader->ether_type)==ETHERTYPE_IPV6)
		item->setText(transportProtocol+":IPv6");
	colorControl();
	table->setItem(count,3,item);
}

void NetworkTraffic::on_stop_clicked()
{
	stop = true;
}

void NetworkTraffic::on_clear_clicked()
{
	clear = true;
	if(stop== true){
		count=0;
		table->setRowCount(count);
	}
}

void NetworkTraffic::on_deviceListWidget_itemClicked(QListWidgetItem *item)
{
	QString selectDevice = item->text();
	pCap(selectDevice);
}

void NetworkTraffic::on_continue_2_clicked()
{
	if(countinueClickNumber>0){
		stop=false;
		threadCreate();
		return;
	}
	countinueClickNumber++;
}
